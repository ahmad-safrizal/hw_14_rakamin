import { Inter } from "next/font/google";
const inter = Inter({ subsets: ["latin"] });
import { VStack, Wrap } from "@chakra-ui/react";
import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import Book from "../components/Books";
const prisma = new PrismaClient();

export default function Homepage(props) {
  return (
    <>
      {books.map((book) => (
        <Book key={`${book.id} ${book.title}`} {...book} />
      ))}
    </>
  );
}

export async function getServerSideProps() {
  try {
    const books = await prisma.book.findMany();
    return {
      props: {
        books
      },
    };
  } catch (err) {
    console.log(err);
    return NextResponse.json(
      { status: 400 },
      { message: "Something went wrong" }
    );
  }
}
